package com.tmobile.deep.streamrouter;

import org.apache.kafka.streams.Topology;

public class StreamSourceTopologyBuilder 
{
	 public static Topology getStreamRouterTopology()
	 {
    	 Topology topology = new Topology();
         topology.addSource(TopologyConstants.STREAM_SOURCE_NAME, TopologyConstants.SOURCE_TOPIC);
         topology.addProcessor(TopologyConstants.STREAM_PROCESSOR,()->new StreamRouterSourceProcessor(),TopologyConstants.STREAM_SOURCE_NAME);
         topology.addSink(TopologyConstants.SINK_1,TopologyConstants.TOPIC_1,TopologyConstants.STREAM_PROCESSOR);
         topology.addSink(TopologyConstants.SINK_2,TopologyConstants.TOPIC_2,TopologyConstants.STREAM_PROCESSOR);
         topology.addSink(TopologyConstants.SINK_3,TopologyConstants.TOPIC_3,TopologyConstants.STREAM_PROCESSOR);
         return topology;
	 }
}