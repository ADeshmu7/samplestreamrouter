package com.tmobile.deep.streamrouter;

public class TopologyConstants {

	public static final String SOURCE_TOPIC="DEEP.EVENT.INCOMINGEVENT";
	public static final String SINK_1="sink1";
	public static final String SINK_2="sink2";
	public static final String SINK_3="sink3";
	public static final String TOPIC_1="DEEP.AD.OUTPUT";
	public static final String TOPIC_2="DEEP.AD.OUTPUT2";
	public static final String TOPIC_3="DEEP.AD.OUTPUT3";
	public static final String STREAM_PROCESSOR="StreamRouterSourceProcessor";
	public static final String STREAM_SOURCE_NAME="mysource";
	public static final String EVENT_NAME="AgggregationTest";
	public static final String STORE_NAME="EventStore";
	public static final String CONSUMER_LIST="sink1~sink2~sink3";
}
