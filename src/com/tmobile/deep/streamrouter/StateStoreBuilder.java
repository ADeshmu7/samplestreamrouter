package com.tmobile.deep.streamrouter;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;


public class StateStoreBuilder implements org.apache.kafka.streams.processor.StateStore
{

    public static StoreBuilder<KeyValueStore<String,String>> getPersistentStateStore() 
    {
        StoreBuilder<KeyValueStore<String,String>> eventsConsumerTopicsStateStore = Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("EventStore"),
                Serdes.String(),
                Serdes.String())
                .withLoggingDisabled();
        return eventsConsumerTopicsStateStore;
    }

    public String name() {
        return null;
    }

    public void init(ProcessorContext context, StateStore root) {
    	
    }

    public void flush() {

    }

    public void close() {

    }

    public boolean persistent() {
        return false;
    }

    public boolean isOpen() {
        return false;
    }
}
