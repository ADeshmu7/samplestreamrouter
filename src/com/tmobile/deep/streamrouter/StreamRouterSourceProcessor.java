package com.tmobile.deep.streamrouter;

import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.To;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamRouterSourceProcessor implements Processor<String, String> 
{

    public ProcessorContext sourceContext;
    private static KeyValueStore<String, String> eventStore;
    public static final Logger LOGGER = LoggerFactory.getLogger(StreamRouterSourceProcessor.class);

    public void init(ProcessorContext sourceContext) 
    {
        this.sourceContext = sourceContext;
        sourceContext.commit();
        eventStore=(KeyValueStore)sourceContext.getStateStore(TopologyConstants.STORE_NAME);
        eventStore.put(TopologyConstants.EVENT_NAME,TopologyConstants.CONSUMER_LIST);
    }

    public void process(String eventName, String eventMessage)
    {
        LOGGER.info("Stream Router Source Processor Process..");
        try
        {
        	if(eventStore.get(eventName)!=null)
        	{
				String[] consumerTopics = eventStore.get(eventName).split("~"); 
			    for(String consumerTopic: consumerTopics) 
				{
					sourceContext.forward(eventName,eventMessage,To.child(consumerTopic)); 
				}
        	}
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        LOGGER.info("Stream Router Processor Process ended.");
    }

    public void close() {
    	
    }
}