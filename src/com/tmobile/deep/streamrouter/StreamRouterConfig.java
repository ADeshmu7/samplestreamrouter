package com.tmobile.deep.streamrouter;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;

public class StreamRouterConfig {

	public static void streamRouterExecute()
	{
		Properties sourceProps = getStreamRouterConfig();
		Topology sourceProcessorTopology = StreamSourceTopologyBuilder.getStreamRouterTopology();
		sourceProcessorTopology.addStateStore(StateStoreBuilder.getPersistentStateStore(),"StreamRouterSourceProcessor");      
	    KafkaStreams sourceStreams = new KafkaStreams(sourceProcessorTopology,sourceProps);
	    sourceStreams.start(); 
	    Runtime.getRuntime().addShutdownHook(new Thread(()-> {
		      sourceStreams.cleanUp();
		    }));
	}
	
	public static Properties getStreamRouterConfig()
    {
    	Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "SourceStreamProcessor");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "devpldeep0004.unix.gsm1900.org:9092");
        props.put(StreamsConfig.STATE_DIR_CONFIG, "streams-pipe");
        props.put("isolation.level", "read_committed"); // ? 
        props.put(StreamsConfig.EXACTLY_ONCE,true);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        return props;
    }
}
